package org.example.framework.security.auth.principal;

import lombok.RequiredArgsConstructor;
import org.example.framework.server.auth.Principal;
import org.example.framework.server.auth.Roles;

@RequiredArgsConstructor
public class LoginPrincipal implements Principal {
  private final String login;
  private static final Roles role = Roles.USER;

  @Override
  public String getName() {
    return login;
  }

  @Override
  public Roles getRole() {
    return role;
  }
}
