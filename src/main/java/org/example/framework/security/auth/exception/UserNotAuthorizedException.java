package org.example.framework.security.auth.exception;

public class UserNotAuthorizedException extends RuntimeException {
    public UserNotAuthorizedException(String userName) {
        super("user " + userName + " not authorized for operation");
    }
}
