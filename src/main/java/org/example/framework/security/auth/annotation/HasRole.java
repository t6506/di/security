package org.example.framework.security.auth.annotation;

import org.example.framework.server.auth.Roles;

import java.lang.annotation.*;

@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface HasRole {
    Roles value();
}
