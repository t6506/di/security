package org.example.framework.security.auth.principal;

import org.example.framework.server.auth.Principal;
import org.example.framework.server.auth.Roles;

public class AnonymousPrincipal implements Principal {

  public static final String ANONYMOUS = "ANONYMOUS";
  private static final Roles role = Roles.ANONYMOUS;

  @Override
  public String getName() {
    return ANONYMOUS;
  }
  @Override
  public Roles getRole() {
    return role;
  }
}
