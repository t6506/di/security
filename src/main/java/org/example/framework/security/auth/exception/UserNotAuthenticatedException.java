package org.example.framework.security.auth.exception;

public class UserNotAuthenticatedException extends RuntimeException {
    public UserNotAuthenticatedException() {
    }

    public UserNotAuthenticatedException(final String message) {
        super(message);
    }

    public UserNotAuthenticatedException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public UserNotAuthenticatedException(final Throwable cause) {
        super(cause);
    }

    public UserNotAuthenticatedException(final String message, final Throwable cause, final boolean enableSuppression,
                                         final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
