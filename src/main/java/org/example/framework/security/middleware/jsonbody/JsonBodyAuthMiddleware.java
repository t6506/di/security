package org.example.framework.security.middleware.jsonbody;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.security.auth.exception.AuthenticationException;
import org.example.framework.security.auth.principal.LoginPrincipal;
import org.example.framework.server.auth.SecurityContext;
import org.example.framework.server.http.Request;
import org.example.framework.server.middleware.Middleware;

import java.net.Socket;
import java.nio.charset.StandardCharsets;


@Slf4j
@RequiredArgsConstructor
public class JsonBodyAuthMiddleware implements Middleware {
  private final Authenticator authenticator;
  private final Gson gson;

  @Override
  public void handle(final Socket socket, final Request request) {
    if (SecurityContext.getPrincipal() != null) {
      return;
    }
    try {
      AuthRQ authRQ = gson.fromJson(new String(request.getBody(), StandardCharsets.UTF_8), AuthRQ.class);
      final String login = authRQ.getLogin();
      final String password = authRQ.getPassword();

      LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(login, password);
      if (!authenticator.authenticate(authRequest)) {
        throw new AuthenticationException("can't authenticate");
      }

      SecurityContext.setPrincipal(new LoginPrincipal(login));
      log.debug("authenticated: {}", login);
    } catch (Exception e) {
      SecurityContext.clear();
      log.error("not authenticated");
    }
  }
}
