package org.example.framework.security.middleware.jsonbody;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class AuthRQ {
    private final String login;
    private final String password;
}
